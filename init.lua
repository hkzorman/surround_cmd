-- Surround command mod by Zorman2000

minetest.register_chatcommand("tnt_surround", {
	privs = {
		interact = true,
		server = true
	},
	func = function(name, param)
		for _,player in pairs(minetest.get_connected_players()) do
			if param == player:get_player_name() then
				local start_pos = vector.round(player:getpos())
				for y = 0, 3 do
					minetest.set_node({x=start_pos.x - 1, y=start_pos.y + y, z=start_pos.z}, {name="tnt:tnt"})
					minetest.set_node({x=start_pos.x + 1, y=start_pos.y + y, z=start_pos.z}, {name="tnt:tnt"})
					minetest.set_node({x=start_pos.x, y=start_pos.y + y, z=start_pos.z - 1}, {name="tnt:tnt"})
					minetest.set_node({x=start_pos.x, y=start_pos.y + y, z=start_pos.z + 1}, {name="tnt:tnt"})
				end
				minetest.set_node({x=start_pos.x, y=start_pos.y-1, z=start_pos.z}, {name="tnt:tnt"})
				minetest.set_node({x=start_pos.x, y=start_pos.y+4, z=start_pos.z}, {name="tnt:tnt"})

				local ignite = function(start_pos)
					minetest.set_node({x=start_pos.x, y=start_pos.y+5, z=start_pos.z}, {name="fire:basic_flame"})
				end

				minetest.after(3, ignite, start_pos)
				return true, "" .. param .. " has been surrounded by TNT!"
			end
		end
		return false, "Unable to find player "..param
	end
})

function go_back_timer(pos)
	local objs = minetest.get_objects_inside_radius(pos, 0.8)
	if not next(objs) then return true end

	for _, obj in pairs(objs) do
		if obj then
			local yaw = 0
			if obj:is_player() then yaw = obj:get_look_horizontal() else yaw = obj:get_yaw() end
			minetest.log("This is yaw: "..dump(yaw))
			if yaw < 0 then
				yaw = yaw + math.pi
			else
				yaw = yaw - math.pi
			end
			minetest.log("This is other yaw: "..dump(yaw))
			local dir = minetest.yaw_to_dir(yaw)
			local target_pos = vector.add(obj:get_pos(), vector.multiply(dir, 10))
			obj:move_to(target_pos, true)
		end
	end
	return true
end

-- Joke node, teleports all entities (including players)
-- 10 steps back in the direction opposite to their walking
minetest.register_node("surround_cmd:go_back", {
	tiles = {"default_grass.png", "default_dirt.png",
		{name = "default_dirt.png^default_grass_side.png",
			tileable_vertical = false}},
	diggable = true,
	description = "Go back",
	on_construct = function(pos)
		local timer = minetest.get_node_timer(pos)
		timer:start(0.2)
	end,
	on_timer = go_back_timer
})
